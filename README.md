# Philosophy
## Running the project
1. Install pipenv: https://docs.pipenv.org
2. Run ```pipenv install```
3. Run ```pipenv run python manage.py runserver```

You should now be able to visit http://127.0.0.1:8000 to see the project