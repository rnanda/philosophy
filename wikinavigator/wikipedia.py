import json
import requests
from urllib.parse import urlparse

from .models import Article
from .crawl import get_next_article_url

_WIKIPEDIA_REST_API_URL = "https://en.wikipedia.org/api/rest_v1"

# Get the title from the wikipedia url
def _get_title(url):
	return urlparse(url).path.split('/')[-1]

# Get the metadata for a page
def get_metadata(url):
	page_title = _get_title(url)
	metadata_query = _WIKIPEDIA_REST_API_URL + "/page/summary/" + page_title
	r = requests.get(metadata_query);
	metadata = json.loads(r.text)

	return metadata

def _article_from_metadata(page_metadata):
	article = Article(page_id=page_metadata['pageid'],
		canonical_title=page_metadata['titles']['canonical'],
		last_revision_id=page_metadata['revision'])

	return article

# Get the next article for the page specified by page_id
def get_next_article(page_metadata):

	page_id = page_metadata['pageid']
	current_article = Article.objects.filter(pk=page_id)

	# if the current article is not in the database
	# make it from metadata and save it
	if not current_article.exists():
		current_article = _article_from_metadata(page_metadata)
		current_article.save()

	else:
		current_article = current_article.first()


	# if the current article has a next article defined and the revision hasn't changed
	# return the next article and its metadata
	if current_article.next_article and current_article.last_revision_id == int(page_metadata['revision']):
		next_article_metadata = get_metadata(current_article.next_article.get_url())
		return (current_article.next_article, next_article_metadata)

	# otherwise if the next article was not defined or the revision has changed
	else:

		# get the next article url and its metadata
		next_article_url = get_next_article_url(current_article.canonical_title)
		next_article_metadata = get_metadata(next_article_url)

		# get the next article from the database if it exists
		next_article = Article.objects.filter(pk=next_article_metadata['pageid'])
		if not next_article.exists():
			# create and save the next article in the database
			next_article = _article_from_metadata(next_article_metadata)
			next_article.save()
		else:
			next_article = next_article.first()
		
		# update the current article revision and next article
		current_article.last_revision_id = page_metadata['revision']
		current_article.next_article = next_article
		current_article.save()

		return (next_article, next_article_metadata)