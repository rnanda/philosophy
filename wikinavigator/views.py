from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader

from . import wikipedia
from .models import Article

_PHILOSPHY_PAGE_ID = 13692155

# Load the home page with a single form
def index(request):
	return render(request, 'wikinavigator/index.html')


# Get the path to philosophy and load the page
def path(request):
	start_url = request.GET['start_url']

	current_page_metadata = wikipedia.get_metadata(start_url)

	# keep track of all visited page ids in a set
	visited_ids = set()

	# keep track of visited page metadatas in order
	page_path = [current_page_metadata]

	# set to true when execution ends in a loop
	ended_in_loop = False

	while True:

		# add current page to visited so we don't visit it again
		visited_ids.add(current_page_metadata['pageid'])

		# get the next article and its metadata
		next_article, next_article_metadata = wikipedia.get_next_article(current_page_metadata)
		page_path.append(next_article_metadata)

		# if we've already visited the next page, break the loop
		if next_article.page_id in visited_ids:
			ended_in_loop = True
			break

		# if the next page is philosophy, break the loop
		elif next_article.page_id == _PHILOSPHY_PAGE_ID:
			break

		current_page_metadata = next_article_metadata

	context = {
		'path': page_path,
		'jumps': len(page_path) - 1,
		'looped': ended_in_loop
	}

	return render(request, 'wikinavigator/path.html', context)