import bs4
from bs4 import BeautifulSoup
import requests

_WIKIPEDIA_DOMAIN = 'https://en.wikipedia.org'

# Loads a webpage
def _load_page(url):
	page = requests.get(url)
	return BeautifulSoup(page.content, 'lxml')
# Finds the first link in an html tag
# accounting for content in parenthesis
def _find_first_link(paragraph):
	parenthesis_active = 0

	for child in paragraph:
		if type(child) is bs4.element.Tag:
			if child.name == 'a':
				if child['href'].startswith('/wiki') and parenthesis_active == 0:
					return child;
			elif parenthesis_active == 0:
				a = _find_first_link(child)
				if a:
					return a

		elif type(child) is bs4.element.NavigableString:
			open_parenthesis = [i for i, char in enumerate(child) if char == '(']
			close_parenthesis = [i for i, char in enumerate(child) if char == ')']

			parenthesis_active += len(open_parenthesis) - len(close_parenthesis)

# Finds the first link in an article by searching <p> tags in order
def _find_first_link_in_article(soup):
	# find all <p> tags in the main content of the page
	paragraphs = soup.find_all('div', class_="mw-parser-output")[0].find_all('p', class_='')
	
	# find the first link in the paragraphs
	for p in paragraphs:
		a = _find_first_link(p)
		
		# if a link is found, return it
		if a:
			return a['href']

# Gets the title of the current article and a link to the next article
def get_next_article_url(canonical_title):
	soup = _load_page(_WIKIPEDIA_DOMAIN + "/wiki/" + canonical_title)

	next_article = _find_first_link_in_article(soup)

	return _WIKIPEDIA_DOMAIN + next_article