from django.urls import path

from . import views

app_name = 'wikinavigator'
urlpatterns = [
    path('', views.index, name='index'),
    path('path', views.path, name="path")
]
