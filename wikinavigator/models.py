from django.db import models

_WIKIPEDIA_DOMAIN = 'https://en.wikipedia.org'

# Create your models here.
class Article(models.Model):
	page_id = models.IntegerField(primary_key=True)
	canonical_title = models.TextField()
	last_revision_id = models.IntegerField()
	next_article = models.ForeignKey('self', 
		on_delete=models.SET_NULL, blank=True, null=True, default=None)

	def get_url(self):
		return _WIKIPEDIA_DOMAIN + "/wiki/" + self.canonical_title