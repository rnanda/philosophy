from django.apps import AppConfig


class WikinavigatorConfig(AppConfig):
    name = 'wikinavigator'
